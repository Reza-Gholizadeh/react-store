import React from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import Navbar from "./Components/Navbar";
import Header from "./Components/Header";
import Products from "./Container/Products";
import Slider from "./Components/Slider";
import "./App.css";
import NotFound from "./Components/NotFound";
import EditProduct from "./Container/EditProduct";
import { useDispatch } from "react-redux";
import { addToCardSuccess } from "./Redux/action";
import Footer from "./Components/Footer";

function App() {
  const dispatch = useDispatch();

  const handleBuyProduct = (e) => {
    dispatch(addToCardSuccess(e));
  };

  return (
    <div>
      <Navbar />
      <Routes>
        <Route
          path="/"
          element={
            <>
              <Slider />
              <Header />
              <Products onClick={handleBuyProduct} />
              <Footer />
            </>
          }
        />
        <Route path="/product/:id" element={<EditProduct />} />
        <Route path="/" element={<Navigate replace to="/" />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
      
    </div>
  );
}

export default App;
