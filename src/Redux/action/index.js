export const SEND_PRODUCT_REQUEST = "SEND_PRODUCT_REQUEST";
export const RECEIVE_PRODUCT_RESPONSE = "RECEIVE_PRODUCT_RESPONSE";
export const RECEIVE_PRODUCT_ERROR = "RECEIVE_PRODUCT_ERROR";
export const ADD_TO_CARD_REQUEST = "ADD_TO_CARD";
export const ADD_TO_CARD_SUCCESS = "ADD_TO_CARD_SUCCESS";
export const REMOVE_FROM_CARD = "REMOVE_FROM_CARD";

export function addToCardRequest(id) {
  return {
    type: ADD_TO_CARD_REQUEST,
    payload: id
};
}
export function addToCardSuccess(data) {
  return {
    type: ADD_TO_CARD_SUCCESS,
    payload: data,
  };
}

export function sendProductRequest(data) {
  return {
    type: SEND_PRODUCT_REQUEST,
    payload: data,
  };
}

export function receiveProductResponse(data) {
  return {
    type: RECEIVE_PRODUCT_RESPONSE,
    payload: data,
  };
}

export function receiveProductError(data) {
  return {
    type: RECEIVE_PRODUCT_ERROR,
    payload: data,
  };
}

export function removeAllItem(data) {
  return {
    type: REMOVE_FROM_CARD,
    payload: data,
  }
}