import { ADD_TO_CARD_REQUEST, ADD_TO_CARD_SUCCESS, REMOVE_FROM_CARD } from "../action";

const init = {
  loading: false,
  data: [],
  count: 0,
};

const addToCard = (state = init, action) => {
  switch (action.type) {
    case ADD_TO_CARD_REQUEST:
      return {loading: true, data: action.payload };
    case ADD_TO_CARD_SUCCESS:
      ////test
      // const theItem = state.data.find(
      //   data => data.name === action.payload,
      // );

      // if(theItem) {
      //   return {
      //     ...state,
      //     data: [...state.data, action.payload],
      //     qty: theItem.qty + 1,
      //   }
      // }
      // return {
      //   loading: false,
      //   data:[...state.data, action.payload],
      //   count: state.count + 1
      // }
      ///end test
     return { loading: false, data: [...state.data, ...action.payload], count: state.count + 1};
    case REMOVE_FROM_CARD:
      return { loading: false, data: [], count: state.count = 0 }
    default:
      return state;
  }
};

export default addToCard;
