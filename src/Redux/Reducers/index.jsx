import addToCard from "./cartReducer";
import productReducer from "./productReducer";
import { combineReducers } from "redux";

const reducer = combineReducers({
  productReducer: productReducer,
  addToCard: addToCard,
});

export default reducer;
