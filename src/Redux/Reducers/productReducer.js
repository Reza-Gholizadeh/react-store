import {
  SEND_PRODUCT_REQUEST,
  RECEIVE_PRODUCT_RESPONSE,
  RECEIVE_PRODUCT_ERROR,
} from "../action";

const init = {
  loading: false,
  data: {},
  error: "",
};

const productReducer = (state = init, action) => {
  switch (action.type) {
    case SEND_PRODUCT_REQUEST:
      return { ...state, loading: true };
    case RECEIVE_PRODUCT_RESPONSE:
      return { loading: false, data: action.payload, error: "" };
    case RECEIVE_PRODUCT_ERROR:
      return { loading: false, data: {}, error: action.payload };
    default:
      return state;
  }
};

export default productReducer;
