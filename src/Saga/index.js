import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import { SEND_PRODUCT_REQUEST, ADD_TO_CARD_REQUEST } from "../Redux/action";
import ProductServis from "../api/productServis";
import {
  receiveProductResponse,
  receiveProductError,
  addToCardSuccess,
} from "../Redux/action";

function* handelGetProduct(action) {
  try {
    const res = yield call(ProductServis.getProductList, action.payload);
    yield put(receiveProductResponse(res.data));
  } catch (error) {
    yield put(receiveProductError(error.message));
  }
}

export function* productSaga() {
  yield takeEvery(SEND_PRODUCT_REQUEST, handelGetProduct);
}

function* handleAddProduct(action) {
  try {
    const res = yield call(ProductServis.getProductList, action.payload);
    yield put(addToCardSuccess(res.data[action.payload - 1]));
  } catch (error) {
    yield put(console.log("error"));
  }
}

export function* cardSaga() {
  yield takeEvery(ADD_TO_CARD_REQUEST, handleAddProduct);
}

export function* rootSaga() {
  yield all([fork(productSaga), fork(cardSaga)]);
}
