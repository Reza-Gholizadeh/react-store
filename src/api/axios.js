import axios from "axios";

const getAxiosInstance = () => {
    return axios.create({
        baseURL: 'http://localhost:8000',
        timeout: 10000
    })
}

export default getAxiosInstance;