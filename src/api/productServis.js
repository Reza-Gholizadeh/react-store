import getAxiosInstance from "./axios";

class ProductServis {
    // start pagination
    static getProductList({page = 1, pageSize = 10, filter}){
        return getAxiosInstance().get(`/product?_page=${page}&_limit=${pageSize}&q=${filter}`)
    }
    // end pagination
    static getProductById(id){
        return getAxiosInstance().get(`/product/${id}`)
    }
    static editProduct(id, product){
        return getAxiosInstance().put(`/product/${id}`, product)
    }
}

export default ProductServis;

//توضیحات static
//The static keyword allows react to get the values of propTypes and defaultProps , without initializing your component

