import { useEffect, useState } from "react";
import ProductItem from "../../Components/ProductItem";
import { useSelector, useDispatch } from "react-redux";
import { sendProductRequest } from "../../Redux/action";
import Spiner from "../../Components/Spiner";
import InfiniteScroll from "react-infinite-scroll-component";
import "./style.css";

const filters = [
  {
    id: 1,
    brand: "samsung",
    value: "samsung",
  },
  {
    id: 2,
    brand: "apple",
    value: "apple",
  },
  {
    id: 3,
    brand: "mi",
    value: "mi",
  },
];

const Products = ({ onClick }) => {
  const [filter, setFilter] = useState("0");
  const products = useSelector((state) => state.productReducer.data);
  const dispatch = useDispatch();
  const [pageSize, setPageSize] = useState(10);

  const fetchData = (pageSize, filter) => {
    setPageSize(pageSize);
    setFilter(filter);
    dispatch(
      sendProductRequest({
        pageSize,
        filter:
          filter !== "0" ?  filter : "0",
          // JSON.stringify({filter:filter})
      })
    );
  };
  useEffect(() => {
    fetchData(pageSize, filter);
    // eslint-disable-next-line
  }, []);

  const hasProduct = products.length > 0;
  const allProduct = !hasProduct ? (
    <Spiner />
  ) : (
    products.map((item) => {
      return (
        <ProductItem
          key={item.id}
          title={item.title}
          imgSrc={item.src}
          price={item.price}
          onClick={() => {
            let array = [];
            array.push(item);
            onClick(array);
            console.log(array);
          }}
          id={item.id}
        />
      );
    })
  );

  const handlechange = (e) => {
    fetchData(pageSize, e.target.value)
  }
  const moreData = () => {
    if (pageSize < 20) {
      return true
    } else {
      return false
    }
  }

  return (
    <>
      <div className="filterBox">

        <select
            className="selectFilter"
            value={filters.value}
            onChange={(e) => handlechange(e)
            }
          >
            <option value={0}>بدون فیلتر</option>
            {filters.map((i) => (
              <option key={i.id} value={i.value}>
                {i.brand}
              </option>
            ))}
          </select>
      </div>
      <section className="products">
        <InfiniteScroll
          dataLength={products?.length || 0} //This is important field to render the next data
          next={() => {
            fetchData(pageSize + 10, filter);
          }}
          hasMore={moreData()}
          loader={<Spiner />}
          endMessage={
            <p style={{ textAlign: "center" }}>
              <b>End of product list</b>
            </p>
          }
          pullDownToRefreshThreshold={50}
        >
          <div className="products-center">{allProduct}</div>
        </InfiniteScroll>
      </section>
    </>
  );
};

export default Products;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//                                                    به جای کد پایین از ریداکس و ریداکس ساگا استفاده کردیم
//------------------------------------------------------------------------------------------------------------
// const [products, setProducts] = useState([]);

// const getItem = async () => {
//   await ProductServis.getProductList()
//     .then((res) => setProducts(res.data))
//     .catch(() => console.log("error"));
// };

// useEffect(() => {
//   getItem();
// }, []);
