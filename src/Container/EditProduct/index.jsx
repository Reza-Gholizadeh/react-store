import React, { useState, useEffect } from "react";
import ProductServis from "../../api/productServis";
import { useParams, useNavigate, Link } from "react-router-dom";
import "./style.css";

const EditProduct = () => {
  const [product, setProduct] = useState([]);
  const params = useParams();
  const navigate = useNavigate();

  const getInputData = async () => {
    const id = params.id;
    await ProductServis.getProductById(id)
      .then((res) => {
        setProduct(res.data);
      })
      .catch(() => console.log("Failed request"));
  };

  useEffect(() => {
    getInputData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setProduct]);
  
  const handleEditedProduct = (e) => {
    const { name, value } = e.target;
    setProduct((previousProduct) => ({
      ...previousProduct,
      [name]: value,
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    e.stopPropagation();
    const id = params.id;
    await ProductServis.editProduct(id, product)
      .then((res) => {
        if (res) {
          navigate("/");
        }
      })
      .catch(() => console.log("feild req"));
  };

  return (
    <div className="edit-box">
      <div className="edit-product">
        <h1 >Edit Product</h1>
        <Link to="/" className="Link">Go Back</Link>
      </div>
      <form className="edit-product-form" onSubmit={handleSubmit}>
        <div className="form-control">
          <input
            name="title"
            type="text"
            defaultValue={product.title}
            onChange={handleEditedProduct}
          />
          <input
            name="price"
            type="number"
            defaultValue={product.price}
            onChange={handleEditedProduct}
          />
        </div>

        <button className="form-btn" type="submit">
          Submit
        </button>
      </form>
    </div>
  );
};

export default EditProduct;
