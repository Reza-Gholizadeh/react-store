import React from "react";
import "./style.css";
import { useSelector, useDispatch } from "react-redux";
import { removeAllItem } from "../../Redux/action";

const ShopingCard = () => {
  const products = useSelector((state) => state.addToCard);
  console.log("product", products);
  const dispatch = useDispatch()

  const handelRemove = () =>{
    dispatch(removeAllItem())
  }

  return (
    <div className="shopingCard">
      <table id="card-content" className="full-weidth table">
        <thead>
          <tr>
            <th>Product</th>
            <th>Price</th>
            <th>QTY</th>
          </tr> 
        </thead>
        <tbody>
          {React.Children.toArray(products?.data?.map((item) => {
            return (
              <tr>
                <td className="title">{item.title}</td>
                <td className="price">{item.price}</td>
                <td className="qty">...</td>
              </tr>
            );
          }))}
        </tbody>
      </table>
      <button type="submit" className="card-btn">Order Confermation</button>
      <button type="submit" className="card-btn" onClick={handelRemove}>
        Delete All Items
      </button>
    </div>
  );
};


export default ShopingCard;
