import "./style.css";

const BuyButton = ({ onClick, btnTitle }) => {
  return (
    <>
      <button className="btn" onClick={onClick}>{btnTitle}</button>
    </>
  );
};

export default BuyButton;
