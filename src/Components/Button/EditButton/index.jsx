const EditButton = ({btnTitle,}) => {
  return (
    <>
      <button className="btn">{btnTitle}</button>
    </>
  );
};

export default EditButton;
