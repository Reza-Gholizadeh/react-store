import ShopingCard from "../ShopingCard";
import "./style.css";
import { useSelector } from "react-redux";


const Navbar = () => {
  const counter = useSelector((state) => state.addToCard.count);

  return (
    <nav className="navbar">
      <div className="navbar-center">
        <span className="nav-icon">
          <i className="fas fa-bars"></i>
        </span>
        <p className="nav-title">Reza Shoping Project</p>
        <span className="nav-shop-icon">
          <i className="fas fa-shopping-cart" ></i>
          <ShopingCard />
          <span className="nav-shop-item">{counter}</span>
        </span>
      </div>
    </nav>
  );
};

export default Navbar;
