import AliceCarousel from "react-alice-carousel";
import "react-alice-carousel/lib/alice-carousel.css";
import "./style.css";
import { useSelector, useDispatch } from "react-redux";
import { sendProductRequest } from "../../Redux/action";
import { useEffect } from "react";
import SliderItem from "./SliderItem";
import Spiner from "../Spiner";

const Slider = () => {
  const products = useSelector((state) => state.productReducer.data);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(sendProductRequest());
  }, [dispatch]);

  const hasProduct = products.length > 0;
  const sliderImage = !hasProduct ? (
    <Spiner />
      ) : (
    products.map((item) => {
      return <SliderItem key={item.id} imgSrc={item.src} />;
    })
  );

  return (
    <div className="sliderimg">
      <AliceCarousel autoPlay autoPlayInterval="1000" infinite={true}>
        {sliderImage}
      </AliceCarousel>
    </div>
  );
};

export default Slider;
