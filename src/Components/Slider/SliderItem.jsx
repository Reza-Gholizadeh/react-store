const SliderItem = ({ imgSrc }) => {
  return (
    <>
      <img src={require("../../assets/img/" + imgSrc)} alt="product" />
    </>
  );
};

export default SliderItem;
