import { Link } from 'react-router-dom';
import './style.css';
import Notfound from '../../assets/img/404.png';

const NotFound = () => {
    return ( 
        <div className='not-found'>
            <h2>Ooops! The Requested Page Not Found!</h2>
            <img src={Notfound} className='not-found-img' alt='404'/>
            <Link className='not-found-link' to="/">Go Home</Link>
        </div>
     );
}
 
export default NotFound;