import React from "react";
import { css } from "@emotion/react";
import ClipLoader from "react-spinners/ClipLoader";

const Spiner = () => {
  const override = css`
    display: block;
    margin: 10% auto;
    border-color: green;
  `;
  return (
    <div>
      <ClipLoader css={override} size={150} />
    </div>
  );
};

export default Spiner;
