import BuyButton from "../Button/BuyButton/index.jsx";
import EditButton from "../Button/EditButton/index.jsx";
import "./style.css";
import { Link } from "react-router-dom";

const ProductItem = ({ title, imgSrc, price, id, onClick }) => {
  return (
    <>
      <section className="product-card">
        <img
          className="product-img"
          src={require("../../assets/img/" + imgSrc)}
          alt="product"
        />
        <div className="product-desc">
          <p className="product-title">{title}</p>
          <p className="product-price">price: {price} $</p>
        </div>
        <BuyButton btnTitle={"Add to card"} onClick={onClick} />
        <Link to={`/product/${id} `}>
          <EditButton btnTitle={"Edit Product"} />
        </Link>
      </section>
    </>
  );
};

export default ProductItem;
