import React from "react";
import "./style.css";

const Footer = () => {
  return (
    <footer className="footer">
      <div className="footer-center">
        <h2 className="footer-header">Reza Shoping Project</h2>
        <p className="footer-desc">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam, quod
          veniam laudantium recusandae distinctio tempore! Nisi vero veniam est
          sequi facere consequuntur provident. Quod sunt, vitae accusantium odit
          ex rem.
        </p>
        <ul className="footer-icons">
          <li>
            <a
              href="https://www.instagram.com/reza_gholiiii/?hl=en"
              target="blank"
            >
              <i className="fa-brands fa-instagram"></i>
            </a>
          </li>
          <li>
            <a
              href="https://www.linkedin.com/in/reza-gholizadeh-279890216?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3B8uqMOpZATai4rnrjK3wVVA%3D%3D"
              target="blank"
            >
              <i className="fa-brands fa-linkedin"></i>
            </a>
          </li>
          <li>
            <a href="#">
              <i className="fa-brands fa-twitter-square"></i>
            </a>
          </li>
          <li>
            <a href="#">
              <i className="fa-brands fa-google-plus-square"></i>
            </a>
          </li>
        </ul>
      </div>
    </footer>
  );
};

export default Footer;
